# Copyright (c) 2017-present, Facebook, Inc.
# All rights reserved.
#
# This source code is licensed under the license found in the LICENSE file in
# the root directory of this source tree. An additional grant of patent rights
# can be found in the PATENTS file in the same directory.

import math

import torch

from fairseq import search, utils
from fairseq.models import FairseqIncrementalDecoder


class SequenceGenerator(object):
    def __init__(
        self, models, tgt_dict, beam_size=1, minlen=1, maxlen=None, stop_early=True,
        normalize_scores=True, len_penalty=1, unk_penalty=0, retain_dropout=False,
        sampling=False, sampling_topk=-1, sampling_temperature=1,
        diverse_beam_groups=-1, diverse_beam_strength=0.5,
    ):
        """Generates translations of a given source sentence.
        Args:
            min/maxlen: The length of the generated output will be bounded by
                minlen and maxlen (not including the end-of-sentence marker).
            stop_early: Stop generation immediately after we finalize beam_size
                hypotheses, even though longer hypotheses might have better
                normalized scores.
            normalize_scores: Normalize scores by the length of the output.
        """
        self.models = models
        self.pad = tgt_dict.pad()
        self.unk = tgt_dict.unk()
        self.eos = tgt_dict.eos()
        self.vocab_size = len(tgt_dict)
        self.beam_size = beam_size
        self.minlen = minlen
        max_decoder_len = min(m.max_decoder_positions() for m in self.models)
        max_decoder_len -= 1  # we define maxlen not including the EOS marker
        self.maxlen = max_decoder_len if maxlen is None else min(maxlen, max_decoder_len)
        self.stop_early = stop_early
        self.normalize_scores = normalize_scores
        self.len_penalty = len_penalty
        self.unk_penalty = unk_penalty
        self.retain_dropout = retain_dropout
        self.tgt_dict = tgt_dict

        assert sampling_topk < 0 or sampling, '--sampling-topk requires --sampling'

        if sampling:
            self.search = search.Sampling(tgt_dict, sampling_topk, sampling_temperature)
        elif diverse_beam_groups > 0:
            self.search = search.DiverseBeamSearch(tgt_dict, diverse_beam_groups, diverse_beam_strength)
        else:
            self.search = search.BeamSearch(tgt_dict)

    def cuda(self):
        for model in self.models:
            model.cuda()
        return self

    def generate_batched_itr(
        self, data_itr, beam_size=None, maxlen_a=0.0, maxlen_b=None,
        cuda=False, timer=None, prefix_size=0,
    ):
        """Iterate over a batched dataset and yield individual translations.
        Args:
            maxlen_a/b: generate sequences of maximum length ax + b,
                where x is the source sentence length.
            cuda: use GPU for generation
            timer: StopwatchMeter for timing generations.
        """
        if maxlen_b is None:
            maxlen_b = self.maxlen

        for sample in data_itr:
            s = utils.move_to_cuda(sample) if cuda else sample
            if 'net_input' not in s:
                continue
            input = s['net_input']
            # model.forward normally channels prev_output_tokens into the decoder
            # separately, but SequenceGenerator directly calls model.encoder
            encoder_input = {
                k: v for k, v in input.items()
                if k != 'prev_output_tokens'
            }
            srclen = encoder_input['src_tokens'].size(1)
            if timer is not None:
                timer.start()
            with torch.no_grad():
                hypos = self.generate(
                    encoder_input,
                    beam_size=beam_size,
                    maxlen=int(maxlen_a*srclen + maxlen_b),
                    prefix_tokens=s['target'][:, :prefix_size] if prefix_size > 0 else None,
                    identifier_tokens=s['identifier_tokens'] if 'identifier_tokens' in s else None,
                    target = s['target']
                )
            if timer is not None:
                timer.stop(sum(len(h[0]['tokens']) for h in hypos))
            for i, id in enumerate(s['id'].data):
                # remove padding
                src = utils.strip_pad(input['src_tokens'].data[i, :], self.pad)
                ref = utils.strip_pad(s['target'].data[i, :], self.pad) if s['target'] is not None else None
                yield id, src, ref, hypos[i]

    def generate(self, encoder_input, beam_size=None, maxlen=None, prefix_tokens=None, identifier_tokens=None, target=None):
        """Generate a batch of translations.

        Args:
            encoder_input: dictionary containing the inputs to
                model.encoder.forward
            beam_size: int overriding the beam size. defaults to
                self.beam_size
            max_len: maximum length of the generated sequence
            prefix_tokens: force decoder to begin with these tokens
        """
        with torch.no_grad():
            return self._generate(encoder_input, beam_size, maxlen, prefix_tokens, identifier_tokens, target)

    def _generate(self, encoder_input, beam_size=None, maxlen=None, prefix_tokens=None, identifier_tokens=None, target=None):
        """See generate"""
        src_tokens = encoder_input['src_tokens']
        bsz, srclen = src_tokens.size()
        maxlen = min(maxlen, self.maxlen) if maxlen is not None else self.maxlen

        # the max beam size is the dictionary size - 1, since we never select pad
        beam_size = beam_size if beam_size is not None else self.beam_size
        beam_size = min(beam_size, self.vocab_size - 1)

        #beam_size = 3

        encoder_outs = []
        incremental_states = {}
        for model in self.models:
            if not self.retain_dropout:
                model.eval()
            if isinstance(model.decoder, FairseqIncrementalDecoder):
                incremental_states[model] = {}
            else:
                incremental_states[model] = None

            # compute the encoder output for each beam
            encoder_out = model.encoder(**encoder_input)
            new_order = torch.arange(bsz).view(-1, 1).repeat(1, beam_size).view(-1)
            new_order = new_order.to(src_tokens.device)
            encoder_out = model.encoder.reorder_encoder_out(encoder_out, new_order)
            encoder_outs.append(encoder_out)

        # initialize buffers
        scores = src_tokens.data.new(bsz * beam_size, maxlen + 1).float().fill_(0)
        scores_buf = scores.clone()
        tokens = src_tokens.data.new(bsz * beam_size, maxlen + 2).fill_(self.pad)
        tokens_buf = tokens.clone()
        tokens[:, 0] = self.eos

        attn, attn_buf = None, None
        nonpad_idxs = None
        #attn = scores.new(bsz * beam_size, src_tokens.size(1), maxlen + 2)
        #attn_buf = attn.clone()
        #nonpad_idxs = src_tokens.ne(self.pad)

        # list of completed sentences
        finalized = [[] for i in range(bsz)]
        finished = [False for i in range(bsz)]
        worst_finalized = [{'idx': None, 'score': -math.inf} for i in range(bsz)]
        num_remaining_sent = bsz

        # number of candidate hypos per step
        cand_size = 2 * beam_size  # 2 x beam size in case half are EOS
        # offset arrays for converting between different indexing schemes
        bbsz_offsets = (torch.arange(0, bsz) * beam_size).unsqueeze(1).type_as(tokens)
        cand_offsets = torch.arange(0, cand_size).type_as(tokens)

        # helper function for allocating buffers on the fly
        buffers = {}

        def buffer(name, type_of=tokens):  # noqa
            if name not in buffers:
                buffers[name] = type_of.new()
            return buffers[name]

        def is_finished(sent, step, unfinalized_scores=None):
            """
            Check whether we've finished generation for a given sentence, by
            comparing the worst score among finalized hypotheses to the best
            possible score among unfinalized hypotheses.
            """
            assert len(finalized[sent]) <= beam_size
            if len(finalized[sent]) == beam_size:
                if self.stop_early or step == maxlen or unfinalized_scores is None:
                    return True
                # stop if the best unfinalized score is worse than the worst
                # finalized one
                best_unfinalized_score = unfinalized_scores[sent].max()
                if self.normalize_scores:
                    best_unfinalized_score /= maxlen ** self.len_penalty
                if worst_finalized[sent]['score'] >= best_unfinalized_score:
                    return True
            return False

        def finalize_hypos(step, bbsz_idx, eos_scores, unfinalized_scores=None):
            """
            Finalize the given hypotheses at this step, while keeping the total
            number of finalized hypotheses per sentence <= beam_size.
            Note: the input must be in the desired finalization order, so that
            hypotheses that appear earlier in the input are preferred to those
            that appear later.
            Args:
                step: current time step
                bbsz_idx: A vector of indices in the range [0, bsz*beam_size),
                    indicating which hypotheses to finalize
                eos_scores: A vector of the same size as bbsz_idx containing
                    scores for each hypothesis
                unfinalized_scores: A vector containing scores for all
                    unfinalized hypotheses
            """
            assert bbsz_idx.numel() == eos_scores.numel()

            # clone relevant token and attention tensors
            tokens_clone = tokens.index_select(0, bbsz_idx)
            tokens_clone = tokens_clone[:, 1:step + 2]  # skip the first index, which is EOS
            tokens_clone[:, step] = self.eos
            attn_clone = attn.index_select(0, bbsz_idx)[:, :, 1:step+2] if attn is not None else None

            # compute scores per token position
            pos_scores = scores.index_select(0, bbsz_idx)[:, :step+1]
            pos_scores[:, step] = eos_scores
            # convert from cumulative to per-position scores
            pos_scores[:, 1:] = pos_scores[:, 1:] - pos_scores[:, :-1]

            # normalize sentence-level scores
            if self.normalize_scores:
                eos_scores /= (step + 1) ** self.len_penalty

            cum_unfin = []
            prev = 0
            for f in finished:
                if f:
                    prev += 1
                else:
                    cum_unfin.append(prev)

            sents_seen = set()
            for i, (idx, score) in enumerate(zip(bbsz_idx.tolist(), eos_scores.tolist())):
                unfin_idx = idx // beam_size
                sent = unfin_idx + cum_unfin[unfin_idx]

                sents_seen.add((sent, unfin_idx))

                def get_hypo():

                    if attn_clone is not None:
                        # remove padding tokens from attn scores
                        hypo_attn = attn_clone[i][nonpad_idxs[sent]]
                        _, alignment = hypo_attn.max(dim=0)
                    else:
                        hypo_attn = None
                        alignment = None

                    return {
                        'tokens': tokens_clone[i],
                        'score': score,
                        'attention': hypo_attn,  # src_len x tgt_len
                        'alignment': alignment,
                        'positional_scores': pos_scores[i],
                    }

                if len(finalized[sent]) < beam_size:
                    finalized[sent].append(get_hypo())
                #if len(finalized[sent]) < 1000:
                #    finalized[sent].append(get_hypo())
                elif not self.stop_early and score > worst_finalized[sent]['score']:
                    # replace worst hypo for this sentence with new/better one
                    worst_idx = worst_finalized[sent]['idx']
                    if worst_idx is not None:
                        finalized[sent][worst_idx] = get_hypo()

                    # find new worst finalized hypo for this sentence
                    idx, s = min(enumerate(finalized[sent]), key=lambda r: r[1]['score'])
                    worst_finalized[sent] = {
                        'score': s['score'],
                        'idx': idx,
                    }

            newly_finished = []
            for sent, unfin_idx in sents_seen:
                # check termination conditions for this sentence
                if not finished[sent] and is_finished(sent, step, unfinalized_scores):
                    finished[sent] = True
                    newly_finished.append(unfin_idx)
            return newly_finished

        reorder_state = None
        #active_scores = None
        batch_idxs = None

        '''process valid identifiers'''
        def get_prefix(token, dictionary):
            prefix, text = '', ''
            stop = False
            for i in range(len(token)-1, 0, -1):
                if dictionary[token[i]] != 'CaMeL' and dictionary[token[i]] != '_':
                    if stop == False:
                        stop = True
                        prefix = dictionary[token[i]] + prefix
                        text =  dictionary[token[i]] + text
                    else:
                        return prefix, text
                else:
                    stop = False
                    prefix = dictionary[token[i]] + prefix
                    if dictionary[token[i]] == '_':
                        text = dictionary[token[i]] + text
            return prefix, text

        if identifier_tokens is not None:
            for i in range(len(identifier_tokens)):
                for k in identifier_tokens[i]['tokens']:
                    identifier_tokens[i]['tokens'][k] += [self.pad, self.unk, self.eos]
                    identifier_tokens[i]['tokens'][k] = torch.LongTensor(identifier_tokens[i]['tokens'][k])
        valid_idx = []
        for i in range(int(tokens.size(0) / beam_size)):
            valid_idx += [i for _ in range(beam_size)]

        length_penalty = {-20: -12.5231, -19: -11.1864, -18: -10.6775, -17: -10.5438, -16: -10.2266, -15: -9.6433, -14: -9.4064,
                          -13: -8.6483, -12: -8.1374, -11: -7.9238, -10: -7.2158, -9: -7.0149, -8: -5.6126, -7: -6.319,
                          -6: -3.7391, -5: -6.0477, -4: -3.7128, -3: -5.5701, -2: -2.4224, -1: -4.2668, 0: 0.0}

        '''
        target = torch.cat([target, torch.zeros(target.size(0), 1).fill_(self.tgt_dict.pad()).long()], dim = 1)
        for i in range(target.size(0)):
            for j in range(target.size(1)):
                if target[i,j] == self.tgt_dict.pad():
                    target[i,j] = self.tgt_dict.eos()
                    break
        '''
        '''
        def expand_beam(old_beam_size, beam_size, valid_idx, encoder_outs, reorder_state, active_scores, scores, scores_buf,
                        tokens, tokens_buf, attn, attn_buf):
            for model in self.models:
                model.make_generation_fast_(
                    beamable_mm_beam_size=beam_size,
                    need_attn=False,
                )

            new_valid_idx = []
            for i in range(0, len(valid_idx), old_beam_size):
                new_valid_idx += [valid_idx[i] for _ in range(beam_size)]
            valid_idx = new_valid_idx

            if encoder_outs is not None:
                size = encoder_outs[0]['encoder_out'][0].size()
                encoder_out = {'encoder_out': (torch.ones(int(size[0] / old_beam_size * beam_size), size[1], size[2]),
                                               torch.ones(int(size[0] / old_beam_size * beam_size), size[1], size[2])),
                               'encoder_padding_mask': torch.ones(int(size[0] / old_beam_size * beam_size),
                                                                  size[1]).byte()
                               if encoder_outs[0]['encoder_padding_mask'] is not None else None}
                for i in range(int(size[0] / old_beam_size)):
                    for j in range(beam_size):
                        encoder_out['encoder_out'][0][i * beam_size + j] = encoder_outs[0]['encoder_out'][0][
                            i * old_beam_size + j % old_beam_size]
                        encoder_out['encoder_out'][1][i * beam_size + j] = encoder_outs[0]['encoder_out'][1][
                            i * old_beam_size + j % old_beam_size]
                        if encoder_outs[0]['encoder_padding_mask'] is not None:
                            encoder_out['encoder_padding_mask'][i * beam_size + j] = \
                                encoder_outs[0]['encoder_padding_mask'][i * old_beam_size + j % old_beam_size]
                encoder_outs = [encoder_out]

            if reorder_state is not None:
                new_reorder_state = torch.ones(int(reorder_state.size(0) / old_beam_size * beam_size))
                for i in range(int(reorder_state.size(0) / old_beam_size)):
                    for j in range(beam_size):
                        new_reorder_state[i * beam_size + j] = reorder_state[i * old_beam_size + j % old_beam_size]
                reorder_state = new_reorder_state.long()

            if active_scores is not None:
                new_active_scores = torch.ones(int(active_scores.size(0) / old_beam_size * beam_size)).fill_(-math.inf)
                for i in range(int(active_scores.size(0) / old_beam_size)):
                    for j in range(old_beam_size):
                        new_active_scores[i * beam_size + j] = active_scores[i * old_beam_size + j % old_beam_size]
                active_scores = new_active_scores

            index, old_index = [], []
            size = scores.size()
            new_scores = src_tokens.data.new(int(size[0]/old_beam_size*beam_size), size[1]).float().fill_(-math.inf)
            for i in range(int(size[0]/old_beam_size)):
                for j in range(old_beam_size):
                    index.append(i * beam_size + j)
                    old_index.append(i * old_beam_size + j % old_beam_size)
            index = torch.LongTensor(index)
            old_index = torch.LongTensor(old_index)
            new_scores[index] = scores[old_index]
            scores = new_scores.clone()
            size = scores_buf.size()
            new_scores_buf = src_tokens.data.new(int(size[0]/old_beam_size*beam_size), size[1]).float().fill_(-math.inf)
            #for i in range(int(size[0]/old_beam_size)):
            #    for j in range(old_beam_size):
            #        new_scores_buf[i * beam_size + j] = scores_buf[i * old_beam_size + j % old_beam_size]
            new_scores_buf[index] = scores_buf[old_index]
            scores_buf = new_scores_buf.clone()

            size = tokens.size()
            new_tokens = src_tokens.data.new(int(size[0]/old_beam_size*beam_size), size[1]).fill_(self.pad)
            #for i in range(int(size[0]/old_beam_size)):
            #    for j in range(old_beam_size):
            #        new_tokens[i * beam_size + j] = tokens[i * old_beam_size + j % old_beam_size]
            new_tokens[index] = tokens[old_index]
            tokens = new_tokens.clone()
            size = tokens_buf.size()
            new_tokens_buf = src_tokens.data.new(int(size[0] / old_beam_size * beam_size), size[1]).fill_(self.pad)
            #for i in range(int(size[0] / old_beam_size)):
            #    for j in range(old_beam_size):
            #        new_tokens_buf[i * beam_size + j] = tokens_buf[i * old_beam_size + j % old_beam_size]
            new_tokens_buf[index] = tokens_buf[old_index]
            tokens_buf = new_tokens_buf.clone()
            tokens[:, 0] = self.eos

            size = attn.size()
            new_attn = scores.new(int(size[0]/old_beam_size*beam_size), size[1], size[2])
            #for i in range(int(size[0] / old_beam_size)):
            #    for j in range(beam_size):
            #        new_attn[i * beam_size + j] = attn[i * old_beam_size + j % old_beam_size]
            new_attn[index] = attn[old_index]
            attn = new_attn.clone()
            size = attn_buf.size()
            new_attn_buf = scores.new(int(size[0] / old_beam_size * beam_size), size[1], size[2])
            #for i in range(int(size[0] / old_beam_size)):
            #    for j in range(beam_size):
            #        new_attn_buf[i * beam_size + j] = attn_buf[i * old_beam_size + j % old_beam_size]
            new_attn_buf[index] = attn_buf[old_index]
            attn_buf = new_attn_buf.clone()

            cand_size = 2 * beam_size  # 2 x beam size in case half are EOS
            # offset arrays for converting between different indexing schemes
            bbsz_offsets = (torch.arange(0, bsz) * beam_size).unsqueeze(1).type_as(tokens)
            cand_offsets = torch.arange(0, cand_size).type_as(tokens)

            return valid_idx, encoder_outs, reorder_state, active_scores, scores, scores_buf, tokens, tokens_buf, \
                   attn, attn_buf, cand_size, bbsz_offsets, cand_offsets
        '''
        '''
        if srclen <= 15:
            expand_steps = [max(1, int(srclen / 2)), max(1, int(srclen / 2.5)), max(1, int(srclen / 3)), max(1, int(srclen / 5))]
        elif srclen < 30:
            expand_steps = [max(1, int(srclen / 1.5)), max(1, int(srclen / 2)), max(1, int(srclen / 3)), max(1, int(srclen / 6))]
        else:
            expand_steps = [max(1, int(srclen / 1.2)), max(1, int(srclen / 1.5)), max(1, int(srclen / 3)), max(1, int(srclen / 7))]
        '''
        for step in range(maxlen + 1):  # one extra step for EOS marker
            # reorder decoder internal states based on the prev choice of beams
            '''
            if step == expand_steps[0]:
                old_beam_size = beam_size
                beam_size = self.beam_size
                valid_idx, encoder_outs, reorder_state, active_scores, scores, scores_buf, tokens, tokens_buf, \
                attn, attn_buf, cand_size, bbsz_offsets, cand_offsets = \
                    expand_beam(old_beam_size, beam_size, valid_idx, encoder_outs, reorder_state, active_scores,
                                scores, scores_buf, tokens, tokens_buf, attn, attn_buf)
            elif step == expand_steps[1]:
                old_beam_size = beam_size
                beam_size = 200
                valid_idx, encoder_outs, reorder_state, active_scores, scores, scores_buf, tokens, tokens_buf, \
                attn, attn_buf, cand_size, bbsz_offsets, cand_offsets = \
                    expand_beam(old_beam_size, beam_size, valid_idx, encoder_outs, reorder_state, active_scores,
                                scores, scores_buf, tokens, tokens_buf, attn, attn_buf)
            elif step == expand_steps[2]:
                old_beam_size = beam_size
                beam_size = 20
                valid_idx, encoder_outs, reorder_state, active_scores, scores, scores_buf, tokens, tokens_buf, \
                attn, attn_buf, cand_size, bbsz_offsets, cand_offsets = \
                    expand_beam(old_beam_size, beam_size, valid_idx, encoder_outs, reorder_state, active_scores,
                                scores, scores_buf, tokens, tokens_buf, attn, attn_buf)
            elif step == expand_steps[3]:
                old_beam_size = beam_size
                beam_size = 10
                valid_idx, encoder_outs, reorder_state, active_scores, scores, scores_buf, tokens, tokens_buf, \
                attn, attn_buf, cand_size, bbsz_offsets, cand_offsets = \
                    expand_beam(old_beam_size, beam_size, valid_idx, encoder_outs, reorder_state, active_scores,
                                scores, scores_buf, tokens, tokens_buf, attn, attn_buf)
            '''

            if reorder_state is not None:
                if batch_idxs is not None:
                    # update beam indices to take into account removed sentences
                    corr = batch_idxs - torch.arange(batch_idxs.numel()).type_as(batch_idxs)
                    reorder_state.view(-1, beam_size).add_(corr.unsqueeze(-1) * beam_size)

                    valid_idx = []
                    for idx in batch_idxs:
                        valid_idx += [idx for _ in range(beam_size)]

                for i, model in enumerate(self.models):
                    if isinstance(model.decoder, FairseqIncrementalDecoder):
                        model.decoder.reorder_incremental_state(incremental_states[model], reorder_state)
                    encoder_outs[i] = model.encoder.reorder_encoder_out(encoder_outs[i], reorder_state)

            lprobs, avg_attn_scores = self._decode(tokens[:, :step + 1], encoder_outs, incremental_states)

            lprobs[:, self.pad] = -math.inf  # never select pad
            lprobs[:, self.unk] -= self.unk_penalty  # apply unk penalty

            '''never select invalid identifiers'''

            if identifier_tokens is not None:
                for i in range(tokens.size(0)):
                    idx = valid_idx[i]
                    prefix, text = get_prefix(tokens[i][ : step + 1], self.tgt_dict)

                    if prefix in identifier_tokens[idx]['tokens']:
                        if prefix != '' and prefix[-5:] != 'CaMeL' and prefix[-1] != '_' and text in identifier_tokens[idx]['text']:
                            tmp = set(identifier_tokens[idx]['tokens'][prefix].tolist()) | \
                                  set(identifier_tokens[idx]['tokens'][''].tolist())
                            tmp = torch.LongTensor(list(tmp))
                        else:
                            tmp = identifier_tokens[idx]['tokens'][prefix]

                    else:
                        tmp = identifier_tokens[idx]['tokens']['']
                    lprobs[i, tmp] += 100  # never select invalid identifiers tokens

                    #lprobs[i, target[idx][step]] += 100

                    '''length penalty (too short, too long)'''
                    '''
                    if step + 1 - int(encoder_input['src_lengths'][idx]) not in length_penalty:
                        penalty = length_penalty[-20]
                    else:
                        penalty = length_penalty[step + 1 - int(encoder_input['src_lengths'][idx])]
                    if step + 1 < int(encoder_input['src_lengths'][idx]):
                        lprobs[i, self.eos] += penalty
                    #else:
                    #    lprobs[i, self.eos] -= penalty
                    '''
            '''
            if step >= 1:
                for i in range(tokens.size(0)):
                    idx = valid_idx[i]
                    lprobs_, indices_ = lprobs[i].topk(100)

                    print('step:', step, 'rank:', i+1, 'score:', round(float(active_scores[i])/step,2), self.tgt_dict.string(tokens[i, :step+1]))
                    print('finalized:', end=' ')
                    for s in finalized[idx]:
                        print(self.tgt_dict.string(s['tokens']), round(s['score'], 2), end = ' ||| ')
                    print()
                    print('target:', self.tgt_dict.string(target[idx]))
                    for j in range(lprobs_.size(0)):
                        print(self.tgt_dict[indices_[j]], round(float(lprobs_[j]), 2), end = ' ||| ')
                    print()
            '''

            # Record attention scores
            if avg_attn_scores is not None:
                if attn is None:
                    attn = scores.new(bsz * beam_size, src_tokens.size(1), maxlen + 2)
                    attn_buf = attn.clone()
                    nonpad_idxs = src_tokens.ne(self.pad)
                attn[:, :, step + 1].copy_(avg_attn_scores)

            scores = scores.type_as(lprobs)
            scores_buf = scores_buf.type_as(lprobs)
            eos_bbsz_idx = buffer('eos_bbsz_idx')
            eos_scores = buffer('eos_scores', type_of=scores)
            if step < maxlen:
                if prefix_tokens is not None and step < prefix_tokens.size(1):
                    probs_slice = lprobs.view(bsz, -1, lprobs.size(-1))[:, 0, :]
                    cand_scores = torch.gather(
                        probs_slice, dim=1,
                        index=prefix_tokens[:, step].view(-1, 1).data
                    ).expand(-1, cand_size)
                    cand_indices = prefix_tokens[:, step].view(-1, 1).expand(bsz, cand_size).data
                    cand_beams = torch.zeros_like(cand_indices)
                else:
                    cand_scores, cand_indices, cand_beams = self.search.step(
                        step,
                        lprobs.view(bsz, -1, self.vocab_size),
                        scores.view(bsz, beam_size, -1)[:, :, :step],
                    )

            else:
                # make probs contain cumulative scores for each hypothesis
                lprobs.add_(scores[:, step - 1].unsqueeze(-1))

                # finalize all active hypotheses once we hit maxlen
                # pick the hypothesis with the highest prob of EOS right now
                torch.sort(
                    lprobs[:, self.eos],
                    descending=True,
                    out=(eos_scores, eos_bbsz_idx),
                )
                num_remaining_sent -= len(finalize_hypos(
                    step, eos_bbsz_idx, eos_scores))
                assert num_remaining_sent == 0
                break

            # cand_bbsz_idx contains beam indices for the top candidate
            # hypotheses, with a range of values: [0, bsz*beam_size),
            # and dimensions: [bsz, cand_size]
            cand_bbsz_idx = cand_beams.add(bbsz_offsets)

            # finalize hypotheses that end in eos
            eos_mask = cand_indices.eq(self.eos)

            finalized_sents = set()
            if step >= self.minlen:
                # only consider eos when it's among the top beam_size indices
                torch.masked_select(
                    cand_bbsz_idx[:, :beam_size],
                    mask=eos_mask[:, :beam_size],
                    out=eos_bbsz_idx,
                )
                if eos_bbsz_idx.numel() > 0:
                    torch.masked_select(
                        cand_scores[:, :beam_size],
                        mask=eos_mask[:, :beam_size],
                        out=eos_scores,
                    )
                    finalized_sents = finalize_hypos(
                        step, eos_bbsz_idx, eos_scores, cand_scores)
                    num_remaining_sent -= len(finalized_sents)

            assert num_remaining_sent >= 0
            if num_remaining_sent == 0:
                break
            assert step < maxlen

            if len(finalized_sents) > 0:
                new_bsz = bsz - len(finalized_sents)

                # construct batch_idxs which holds indices of batches to keep for the next pass
                batch_mask = cand_indices.new_ones(bsz)
                batch_mask[cand_indices.new(finalized_sents)] = 0
                batch_idxs = batch_mask.nonzero().squeeze(-1)

                eos_mask = eos_mask[batch_idxs]
                cand_beams = cand_beams[batch_idxs]
                bbsz_offsets.resize_(new_bsz, 1)
                cand_bbsz_idx = cand_beams.add(bbsz_offsets)

                cand_scores = cand_scores[batch_idxs]
                cand_indices = cand_indices[batch_idxs]
                if prefix_tokens is not None:
                    prefix_tokens = prefix_tokens[batch_idxs]

                scores = scores.view(bsz, -1)[batch_idxs].view(new_bsz * beam_size, -1)
                scores_buf.resize_as_(scores)
                tokens = tokens.view(bsz, -1)[batch_idxs].view(new_bsz * beam_size, -1)
                tokens_buf.resize_as_(tokens)
                if attn is not None:
                    attn = attn.view(bsz, -1)[batch_idxs].view(new_bsz * beam_size, attn.size(1), -1)
                    attn_buf.resize_as_(attn)
                bsz = new_bsz
            else:
                batch_idxs = None

            # set active_mask so that values > cand_size indicate eos hypos
            # and values < cand_size indicate candidate active hypos.
            # After, the min values per row are the top candidate active hypos
            active_mask = buffer('active_mask')
            torch.add(
                eos_mask.type_as(cand_offsets) * cand_size,
                cand_offsets[:eos_mask.size(1)],
                out=active_mask,
            )

            # get the top beam_size active hypotheses, which are just the hypos
            # with the smallest values in active_mask
            active_hypos, _ignore = buffer('active_hypos'), buffer('_ignore')
            torch.topk(
                active_mask, k=beam_size, dim=1, largest=False,
                out=(_ignore, active_hypos)
            )

            active_bbsz_idx = buffer('active_bbsz_idx')

            torch.gather(
                cand_bbsz_idx, dim=1, index=active_hypos,
                out=active_bbsz_idx,
            )
            active_scores = torch.gather(
                cand_scores, dim=1, index=active_hypos,
                out=scores[:, step].view(bsz, beam_size),
            )

            active_bbsz_idx = active_bbsz_idx.view(-1)
            active_scores = active_scores.view(-1)

            # copy tokens and scores for active hypotheses
            torch.index_select(
                tokens[:, :step + 1], dim=0, index=active_bbsz_idx,
                out=tokens_buf[:, :step + 1],
            )
            torch.gather(
                cand_indices, dim=1, index=active_hypos,
                out=tokens_buf.view(bsz, beam_size, -1)[:, :, step + 1],
            )
            if step > 0:
                torch.index_select(
                    scores[:, :step], dim=0, index=active_bbsz_idx,
                    out=scores_buf[:, :step],
                )
            torch.gather(
                cand_scores, dim=1, index=active_hypos,
                out=scores_buf.view(bsz, beam_size, -1)[:, :, step],
            )

            # copy attention for active hypotheses
            if attn is not None:
                torch.index_select(
                    attn[:, :, :step + 2], dim=0, index=active_bbsz_idx,
                    out=attn_buf[:, :, :step + 2],
                )

            # swap buffers

            tokens, tokens_buf = tokens_buf, tokens
            scores, scores_buf = scores_buf, scores
            if attn is not None:
                attn, attn_buf = attn_buf, attn

            # reorder incremental state in decoder
            reorder_state = active_bbsz_idx

        # sort by score descending
        for sent in range(len(finalized)):
            finalized[sent] = sorted(finalized[sent], key=lambda r: r['score'], reverse=True)

        return finalized

    def _decode(self, tokens, encoder_outs, incremental_states):
        if len(self.models) == 1:
            return self._decode_one(tokens, self.models[0], encoder_outs[0], incremental_states, log_probs=True)

        log_probs = []
        avg_attn = None
        for model, encoder_out in zip(self.models, encoder_outs):
            probs, attn = self._decode_one(tokens, model, encoder_out, incremental_states, log_probs=True)
            log_probs.append(probs)
            if attn is not None:
                if avg_attn is None:
                    avg_attn = attn
                else:
                    avg_attn.add_(attn)
        avg_probs = torch.logsumexp(torch.stack(log_probs, dim=0), dim=0) - math.log(len(self.models))
        if avg_attn is not None:
            avg_attn.div_(len(self.models))
        return avg_probs, avg_attn

    def _decode_one(self, tokens, model, encoder_out, incremental_states, log_probs):
        with torch.no_grad():
            if incremental_states[model] is not None:
                #print(encoder_out['encoder_out'][0].size(), encoder_out['encoder_out'][1].size(), encoder_out['encoder_padding_mask'].size())
                decoder_out = list(model.decoder(tokens, encoder_out, incremental_state=incremental_states[model]))

            else:
                #print(tokens.size(), encoder_out['encoder_out'][0].size(), encoder_out['encoder_out'][1].size())
                decoder_out = list(model.decoder(tokens, encoder_out))

            decoder_out[0] = decoder_out[0][:, -1, :]
            attn = decoder_out[1]
            if type(attn) is dict:
                attn = attn['attn']
            if attn is not None:
                if type(attn) is dict:
                    attn = attn['attn']
                attn = attn[:, -1, :]
        probs = model.get_normalized_probs(decoder_out, log_probs=log_probs)
        return probs, attn
